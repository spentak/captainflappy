﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour {

	[SerializeField] GameObject cloudPrefab;

	float maxYPos = 6.22575f;
	float minYPos = 2.759591f;
	float createXPos = 7.631213f;
	float destroyXPos = -6.543679f;
	float speed;
	bool objectActive;

	void Awake()
	{
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_END_GAME);
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_START_GAME);
	}

	// Use this for initialization
	void Start () 
	{
		DebugUtil.Assert(cloudPrefab != null);
		speed = Random.Range(1,6);
		objectActive = true;
	}

	public void NotifStartGame()
	{
		objectActive = true;
	}

	public void NotifEndGame()
	{
		objectActive = false;
	}

	// Update is called once per frame
	void Update () {

		if (objectActive)
		transform.Translate(Vector3.left  * speed * Time.deltaTime);

		if (transform.localPosition.x <= destroyXPos)
		{
			GameObject go = Instantiate(cloudPrefab) as GameObject;
			float newY = Random.Range(maxYPos,minYPos);
			go.transform.localPosition = new Vector3(createXPos,newY,go.transform.position.z);
			Destroy(gameObject);
		}
	
	}
}
