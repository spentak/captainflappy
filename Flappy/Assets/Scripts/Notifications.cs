﻿
using System.Collections;

public class Notifications 
{

	public const string NOTIF_START_GAME = "NotifStartGame";
	public const string NOTIF_PAUSE_GAME = "NotifPauseGame";
	public const string NOTIF_END_GAME = "NotifEndGame";
	public const string NOTIF_RESTART_GAME = "NotifRestartGame";
	public const string NOTIF_ADD_COIN = "NotifAddCoin";
	public const string NOTIF_MAIN_SCREEN = "NotifMainScreen";
	public const string NOTIF_FIRST_TAP = "NotifFirstTap";
}
