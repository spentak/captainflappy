﻿using UnityEngine;
using System.Collections;

public class Tombstone : MonoBehaviour {

	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowTombstone()
	{
		int rand = Random.Range (0, 4);

		if (rand == 1)
				gameObject.SetActive (true);
		else
				gameObject.SetActive (false);
	}
}
