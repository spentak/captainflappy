﻿using UnityEngine;

using System.Collections;


[RequireComponent(typeof(Collider))]
public class Button : MonoBehaviour 
{
	public static string buttonPressed = "ButtonPressed";
	public static string buttonUp = "ButtonUp";

	[SerializeField]bool scaleOnPress;
	[SerializeField]GameObject buttonListener;
	[SerializeField]string buttonName;
	[SerializeField]Vector3 scaleUpSize;
	[SerializeField]Vector3 scaleDownSize;

	void Awake()
	{
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_RESTART_GAME);
		gameObject.tag = "button";
	}

	void Start()
	{       
		DebugUtil.Assert (buttonListener != null); 
		DebugUtil.Assert (buttonName != "" && buttonName != null);
		DebugUtil.Assert (buttonName == transform.name);

		if (scaleOnPress)
		{
			//DebugUtil.Assert (scaleUpSize != null);
			//DebugUtil.Assert (scaleDownSize != null);
		}
	}

	RaycastHit hit;
	Ray touchRay;
	bool btnDown;
	void Update()
	{
		//Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);
		//if (Physics.Raycast(ray, out hit) && Input.GetMouseButtonDown(0) && hit.transform.name == "Play")
			//if(hit.transform.name == "Play")
		if (Input.GetMouseButtonDown(0))
		{
			touchRay = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast(touchRay, out hit))
			{
				if (hit.transform.tag == "button")
				{
					if (hit.transform.name == buttonName)
					{
						btnDown = true;
						ButtonDown();
					}
				}
			}
		}
		else if (Input.GetMouseButtonUp(0))
		{
			if (btnDown)
			{
				btnDown = false;
				ButtonUp();
			}
		}


	}

	void ButtonUp()
	{
		if (scaleOnPress)
			gameObject.transform.localScale = scaleDownSize;

		buttonListener.SendMessage(buttonPressed, buttonName);
	}
	
	void ButtonDown()
	{
		if (scaleOnPress)
			gameObject.transform.localScale = scaleUpSize;

	}

	public void NotifRestartGame()
	{
		if (scaleOnPress)
		gameObject.transform.localScale = scaleDownSize;
	}
	
	
	
	
}