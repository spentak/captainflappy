﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour {


	bool jump;
	bool gameOver;
	float jumpForce = 500.0f;
	float jetpackForce = 25.0f;
	float bikeForce = 5.0f;
	float maxVelocity = 6.0f;
	float gravity = 2.61f;
	float xPos = -1.88f;
	Vector3 characterStartPos = new Vector3(-1.88f,0.2905934f,0);
	tk2dSpriteAnimationClip animClip;
	private const float jetpackGravityScale = 4;



	void Awake()
	{
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_RESTART_GAME);
	}

	// Use this for initialization
	void Start () 
	{
		SetCharacter();
		AnimateCharacter();
	}

	public void AnimateCharacter()
	{
		//iTween.MoveTo(gameObject,iTween.Hash("y",-3.2549f,"time",.5f,"name","charanimgame","looptype",iTween.LoopType.pingPong,"easetype",iTween.EaseType.easeInOutBack));
	}

	void SetCharacter()
	{
		rigidbody2D.gravityScale = 0;
		collider2D.enabled = true;
	}

	float curY;
	float startJumpY;
	void FixedUpdate ()
	{
		//Debug.Log("Velocity: " + rigidbody2D.velocity.y);

		if (jump == true)
		{
			rigidbody2D.AddForce(new Vector2(0f,jumpForce));
			jump = false;
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x,maxVelocity);
			curY = rigidbody2D.transform.position.y;
			startJumpY = curY;
		}



		curY = rigidbody2D.transform.position.y;

			
		if (curY < startJumpY)
			rigidbody2D.transform.rotation = Quaternion.Lerp(transform.rotation,Quaternion.Euler(0,0,-33.36f),Time.deltaTime * 10);
		
		else if (curY >= startJumpY)
			rigidbody2D.transform.rotation = Quaternion.Lerp(transform.rotation,Quaternion.Euler(0,0,28.4f),Time.deltaTime * 10);

		if (rigidbody2D.velocity.y < 1)
			rigidbody2D.transform.rotation = Quaternion.Lerp(transform.rotation,Quaternion.Euler(0,0,0f),Time.deltaTime * 10);


		gameObject.transform.position = new Vector3(xPos,gameObject.transform.position.y,gameObject.transform.position.z);
	}

	bool firstTap;

	public void HandleInput(bool buttonDown)
	{
		if (gameOver)
			return;

		if (!firstTap) 
		{
			firstTap = true;
			NotificationCenter.DefaultCenter.PostNotification (this, Notifications.NOTIF_FIRST_TAP);
			iTween.StopByName("charanimgame");
		}

		if (buttonDown) 
		{
			Jump();
		}

	}

	void Jump()
	{
		if (rigidbody2D.gravityScale <= 0)
			rigidbody2D.gravityScale = gravity;
		
		PlayerAnimations anims = GetComponentInChildren<PlayerAnimations>();

		if (anims != null)
			anims.StartCoroutine("JumpAnim");

		jump = true;
		SoundManager.Instance.PlaySoundEffect(SoundManager.Instance.sfxJump);
	}
	

	public void ButtonUp(string buttonName)
	{
		jump = false;
	}


	void OnCollisionEnter2D(Collision2D collision)
	{
		if (gameOver)
			return;
		if (collision.collider.tag == "roof")
			return;

		rigidbody2D.AddForce(new Vector2(-500f,500f));
		rigidbody2D.AddTorque(500f);
		StartCoroutine(TurnOffCollider());
		gameOver = true;
		NotificationCenter.DefaultCenter.PostNotification(this,Notifications.NOTIF_END_GAME);
		SoundManager.Instance.StopAllMusic();
		SoundManager.Instance.PlaySoundEffect(SoundManager.Instance.sfxCrash);
	}

	IEnumerator TurnOffCollider()
	{
		yield return new WaitForSeconds(0.4f);
		collider2D.enabled = false;
	}

	bool isTriggered;
	IEnumerator ResetTrigger()
	{
		yield return new WaitForSeconds(1.0f);
		isTriggered = false;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (!isTriggered)
		{
			Destroy(other.gameObject);
			NotificationCenter.DefaultCenter.PostNotification(this,Notifications.NOTIF_ADD_COIN);
			SoundManager.Instance.PlaySoundEffect(SoundManager.Instance.sfxAddPoint);
			GameManager.Instance.AddScore();
			isTriggered = true;
			StartCoroutine(ResetTrigger());
		}

	}

	public void NotifMainScreen()
	{
		gameObject.transform.position = characterStartPos;
	}

	public void NotifRestartGame()
	{
		gameOver = false;
		gameObject.transform.position = characterStartPos;
		SetCharacter();
		AnimateCharacter();
		firstTap = false;
	}


}
