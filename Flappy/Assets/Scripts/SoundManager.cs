﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour 
{
	public AudioSource audioSource;
	public AudioSource musicSource;
	
	
	public AudioClip musicMain;

	public AudioClip sfxGameOver;
	public AudioClip sfxJump;
	public AudioClip sfxAddPoint;
	public AudioClip sfxPlayButton;
	public AudioClip sfxCrash;
	public AudioClip sfxShip;

	//These are needed for the options menu
	public static bool SoundOn = true;
	public static bool MusicOn = true;
	private bool sfxPlaying;
	private static SoundManager instance;
	
	public static SoundManager Instance {
		get {
			return instance;
		}
		
	}	
	
	void Awake()
	{
		instance = this;
	}
	
	// Use this for initialization
	void Start () 
	{
		
		DebugUtil.Assert (instance != null);
		DebugUtil.Assert (musicMain != null);
		DebugUtil.Assert (sfxGameOver != null);
		DebugUtil.Assert (sfxJump != null);
		DebugUtil.Assert (sfxAddPoint != null);
		DebugUtil.Assert (sfxPlayButton != null);
		DebugUtil.Assert (sfxCrash != null);
		DebugUtil.Assert (sfxShip != null);

		
		SoundManager.SoundOn = true;
		SoundManager.MusicOn = true;
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
	}

	IEnumerator SFXDelay(float waitForSeconds)
	{
		yield return new WaitForSeconds(waitForSeconds + .01f);
		sfxPlaying = false;
	}

	public void PlaySoundEffectSafely(AudioClip source)
	{
		if (!SoundOn)
			return;
		if (sfxPlaying)
			return;
		
		sfxPlaying = true;
		audioSource.clip = source;
		audioSource.Play();
		StartCoroutine(SFXDelay(source.length));
	}
	
	public void PlaySoundEffect(AudioClip source)
	{
		if (!SoundManager.SoundOn)
			return;

		bool playNewSound = true;

		if (audioSource.isPlaying)
		{
			if (audioSource.clip.name == "endgame")
				playNewSound = false;
			if (source.name == "endgame")
				playNewSound = true;
			else if (source.name == "die")
				playNewSound = true;
			else if (audioSource.clip.name == "die")
				playNewSound = false;
			else if (source.name == "laugh")
				playNewSound = true;
			else if (audioSource.clip.name == "laugh")
				playNewSound = false;
			else if (audioSource.clip.name == "pumpkin")
				playNewSound = false;
			else if (source.name == "pumpkin")
				playNewSound = true;

		}	


		if (playNewSound)
		{
			audioSource.clip = source;
			audioSource.Play();
		}
	}
	
	public void PlayMusic(AudioClip source)
	{
		musicSource.clip = source;
		
		
		if (SoundManager.MusicOn) 
			musicSource.Play ();
		
	}

	public void StopSoundFX()
	{
		audioSource.Stop();
	}
	
	public void StopMusic(AudioClip source)
	{
		musicSource.clip = source;
		musicSource.Stop();
	}
	
	public void StopAllMusic()
	{
		musicSource.Stop();	
	}
	
	public void ResumeMusic()
	{
		musicSource.Play();	
	}
	
	
}