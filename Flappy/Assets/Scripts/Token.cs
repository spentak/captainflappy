﻿using UnityEngine;
using System.Collections;

public class Token: MonoBehaviour {
	
	private float destroyXPos = -10.0f;
	private float startXPos = 10.0f;
	private float maxYPos = 6.1f;
	private float minYPos = -5.0f;
	private bool moveLeft;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine(DelayTokenMovement (60.0f));
	}

	// Update is called once per frame
	void Update () 
	{
		if (moveLeft)
		{
			if (GameManager.GameActive == true)
				transform.Translate(Vector3.left  * ((ObstacleManager.obstacleSpeed * GameManager.SpeedMultiplier) * Time.deltaTime));
		}
		if (transform.position.x <= destroyXPos)
		{
			ResetToken();
		}
	}

	IEnumerator DelayTokenMovement(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		moveLeft = true;
	}
	
	void ResetToken()
	{
		moveLeft = false;
		transform.position = new Vector3(startXPos,Random.Range(minYPos,maxYPos),transform.position.z);
		gameObject.renderer.enabled = true;
		StartCoroutine(DelayTokenMovement(120.0f));
	}
}
