﻿using UnityEngine;
using System.Collections;

public class PlayerAnimations : MonoBehaviour {
	Animation animation;
	[SerializeField] bool playIntroAnims;
	void Awake()
	{
		animation = GetComponent<Animation> ();
	}

	// Use this for initialization
	void Start () {
		animation.wrapMode = WrapMode.Loop;
		animation.Play ();

		if (playIntroAnims)
		StartCoroutine(StartIdle());
	
	}

	void ChooseAnim()
	{
		int rand = Random.Range(0,2);

		if (rand == 0)
			StartCoroutine(StartPanic());
		else
			StartCoroutine(StartDizzy());
	}

	IEnumerator StartPanic()
	{
		iTween.MoveTo(gameObject,iTween.Hash("y",-3.4204f,"time",0.7f));
		animation.CrossFade("panic",0.3f);
		yield return new WaitForSeconds(1.3f);
		iTween.MoveTo(gameObject,iTween.Hash("y",-6.341f,"time",0.5f,"easetype",iTween.EaseType.easeInBack));
		StartCoroutine(StartIdle());

	}

	IEnumerator StartIdle()
	{
		animation.CrossFade("idle2",0.3f);
		yield return new WaitForSeconds(5.0f);
		ChooseAnim();
	}

	IEnumerator StartDizzy()
	{
		animation.CrossFade("dizzy",0.3f);
		yield return new WaitForSeconds(5.0f);
		StartCoroutine(StartIdle());
	}

	public IEnumerator JumpAnim()
	{
		animation.wrapMode = WrapMode.Once;
		animation.Play("jump");
		yield return new WaitForSeconds(0.2f);
		animation.Play("transFall");
		yield return new WaitForSeconds(0.15f);
		animation.CrossFade("idle2",0.3f);

	}

	// Update is called once per frame
	void Update () {
	
	}
}
