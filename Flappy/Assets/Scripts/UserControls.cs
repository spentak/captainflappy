﻿using UnityEngine;

using System.Collections;


[RequireComponent(typeof(Collider))]
public class UserControls : MonoBehaviour 
{
	[SerializeField] Character character;
	
	void Awake()
	{
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_RESTART_GAME);
	}
	
	void Start()
	{       
		DebugUtil.Assert (character != null);
	}
	
	RaycastHit hit;
	Ray touchRay;
	bool btnDown;
	
	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			touchRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			
			if (Physics.Raycast(touchRay, out hit))
			{
				if (hit.transform.tag == "screentap")
				{
					btnDown = true;
					character.HandleInput(true);
				}
			}
		}
		else if (Input.GetMouseButtonUp(0))
		{
			if (btnDown)
			{
				btnDown = false;
				character.HandleInput(false);
			}
		}

	}

	
	
	public void NotifRestartGame()
	{

	}
	
	
	
	
}