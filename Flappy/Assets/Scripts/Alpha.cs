﻿using UnityEngine;
using System.Collections;

public class Alpha : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Color color = renderer.material.color;
		color.a = 0.7f;
		renderer.material.color = color;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
