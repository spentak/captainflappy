﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms.GameCenter;

public class LeaderboardManager : MonoBehaviour
{
	#if UNITY_IPHONE
	const string ScoreLeaderboard = "com.bigkahunagames.tikibirdflyerLeaderboardID";
	
	void Start()
	{
		//Log in to Game Center
		StartCoroutine(delayGCAuth(false));
	}
	
	IEnumerator delayGCAuth(bool show)
	{
		yield return new WaitForSeconds(1);
		GCAuth(show);
	}
	#endif
	
	public static void GCAuth(bool showLeaderboards)
	{
		#if UNITY_IPHONE
		Social.localUser.Authenticate(success =>
		                              {
			if(success)
			{
				if(showLeaderboards)
					Social.ShowLeaderboardUI();
				
				Debug.Log("GC: Auth succeeded");
			}
			else
			{
				Debug.Log("GC: Auth failed");
			}
		});
		#endif
	}
	
	public static void ShowLeaderboards()
	{
		#if UNITY_IPHONE
		if(Social.localUser.authenticated)
			Social.ShowLeaderboardUI();
		else
			GCAuth(true);
		
		Debug.Log("GC: Leaderboards Pressed");
		#endif
	}
	
	public static void ReportScore(int score)
	{
		#if UNITY_IPHONE
		Debug.Log("GC: SCORE: " + score.ToString());
		
		if(Social.localUser.authenticated)
		{
			Social.ReportScore(score, ScoreLeaderboard, success =>
			                   {
				Debug.Log("GC: Reporting score was successful? " + success);
			});
		}
		#endif
	}
	
	public static void ReportCustomers(int cust)
	{
		#if UNITY_IPHONE
		Debug.Log("GC: CUSTOMERS: " + cust.ToString());
		
		if(Social.localUser.authenticated)
		{
			Social.ReportScore(cust, ScoreLeaderboard, success =>
			                   {
				Debug.Log("GC: Reporting customers was successful? " + success);
			});
		}
		#endif
	}
}