﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {


	float pipeDestroyXPos = -8.476689f;
	float groundDestroyXPos = -16.752f;
	
	[SerializeField] bool isGroundObstacle;

	bool obstacleActive;
	Vector3 groundStartPos = new Vector3(-6.0f,-8.4397f,-0.49296f);
	Renderer renderer;

	Tombstone tombstone;
	Cauldron cauldron;
	Tree tree;

	public Vector3 GroundStartPos {
		get {
			return groundStartPos;
		}
	}

	void Awake()
	{

		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_START_GAME);
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_END_GAME);
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_RESTART_GAME);
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_MAIN_SCREEN);
		renderer = GetComponentInChildren<Renderer> ();
	}

	// Use this for initialization
	void Start () 
	{
		tombstone = GetComponentInChildren<Tombstone> ();
		cauldron = GetComponentInChildren<Cauldron> ();
		tree = GetComponentInChildren<Tree> ();

		if (GameManager.GameActive == true)
			SetObstacleActive (true);

		InitExtras ();
	}

	public void NotifFirstTap()
	{
		SetObstacleActive (true);
	}

	public void NotifStartGame()
	{
		if (isGroundObstacle)
			SetObstacleActive (true);
	}

	public void NotifEndGame()
	{
		SetObstacleActive (false);
	}

	void SetObstacleActive(bool active)
	{
		obstacleActive = active;
	}

	// Update is called once per frame
	void Update () 
	{
		if (obstacleActive == false || GameManager.GameActive == false)
			return;

		transform.Translate(Vector3.left  * ((ObstacleManager.obstacleSpeed * GameManager.SpeedMultiplier) * Time.deltaTime));

		if (isGroundObstacle)
		{
			if (transform.localPosition.x <= groundDestroyXPos)
			{
				Vector3 newPos = new Vector3(transform.position.x + (renderer.bounds.size.x * ObstacleManager.totalGroundPieces),groundStartPos.y,groundStartPos.z);
				transform.position = newPos;

				if (tombstone != null)
				tombstone.ShowTombstone();

				if (cauldron != null)
					cauldron.ShowCauldron();

				if (tree != null)
					tree.ShowTree();
			}

		}

		if (!isGroundObstacle)
		{
			if (transform.position.x <= pipeDestroyXPos)
			{
				Destroy(gameObject);
			}
		}
	}

	public void DestroyObject()
	{
		Destroy(gameObject);
	}

	public void NotifRestartGame()
	{
		DestroyObject();
	}

	public void NotifMainScreen()
	{
		DestroyObject();
	}
	
	void OnDestroy()
	{
		//NotificationCenter.DefaultCenter.RemoveObserver(this,Notifications.NOTIF_RESTART_GAME);
		//NotificationCenter.DefaultCenter.RemoveObserver(this,Notifications.NOTIF_START_GAME);
	}

	public void InitExtras()
	{
		if (tombstone != null)
			tombstone.ShowTombstone ();

		if (cauldron != null)
			cauldron.ShowCauldron ();

		if (tree != null)
			tree.ShowTree ();
	}
}
