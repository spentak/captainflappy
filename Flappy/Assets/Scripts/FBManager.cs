﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FBManager : MonoBehaviour {

	private string lastResponse;

	// Use this for initialization
	void Start () 
	{
		CallFBInit();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	#region FB.Init() example
	
	private bool isInit = false;
	
	public void CallFBInit()
	{
		FB.Init(OnInitComplete, OnHideUnity);
	}
	
	private void OnInitComplete()
	{
		Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
		isInit = true;

	}
	
	private void OnHideUnity(bool isGameShown)
	{
		Debug.Log("Is game showing? " + isGameShown);
	}
	
	#endregion

	#region FB.Login() example
	
	private void CallFBLogin()
	{
		FB.Login("email,publish_actions", LoginCallback);
	}
	
	void LoginCallback(FBResult result)
	{
		if (result.Error != null)
			lastResponse = "Error Response:\n" + result.Error;
		else if (!FB.IsLoggedIn) {
			lastResponse = "Login cancelled by Player";
		}
		else
		{
			//CallFBFeed();
		}
	}
	
	private void CallFBLogout()
	{
		FB.Logout();
	}
	#endregion

	#region FB.Feed() example
	
	public string FeedToId = "";
	public string FeedLink = "http://x.co/TikiBird";
	public string FeedLinkName = "Tiki Bird Flyer";
	public string FeedLinkCaption = "Click to Download";
	public string FeedLinkDescription = "HEY CHECK IT OUT! I SCORED XXX IN TIKI BIRD FLYER! IT'S SUPER ADDICTING! @BigKahunaGames #games #iphone";
	public string FeedPicture = "";
	public string FeedMediaSource = "";
	public string FeedActionName = "";
	public string FeedActionLink = "";
	public string FeedReference = "";
	public bool IncludeFeedProperties = false;
	private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();
	
	public void CallFBFeed()
	{
		Dictionary<string, string[]> feedProperties = null;
		if (IncludeFeedProperties)
		{
			feedProperties = FeedProperties;
		}
		FB.Feed(
			toId: FeedToId,
			link: FeedLink,
			linkName: FeedLinkName,
			linkCaption: FeedLinkCaption,
			linkDescription: FeedLinkDescription,
			picture: FeedPicture,
			mediaSource: FeedMediaSource,
			actionName: FeedActionName,
			actionLink: FeedActionLink,
			reference: FeedReference,
			properties: feedProperties,
			callback: Callback
			);
	}
	
	#endregion

	void Callback(FBResult result)
	{
		if (result.Error != null)
			lastResponse = "Error Response:\n" + result.Error;
		else
		{
			lastResponse = "Success Response:\n";
		}
	}

	public void ButtonPressed(string buttonName)
	{
		Debug.Log("FBFBFB");
		if (buttonName == "facebookButton")
		{
			if (FB.IsLoggedIn)
				CallFBFeed();
			else
				CallFBLogin();
		}
	}
}
