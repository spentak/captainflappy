﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

	private const float firstDelay = 5.0f;
	private const float startXPos = 0f;
	private const float endXPos = -50.588f;
	private const float maxYPos = 0f;
	private const float minYPos = -10.644f;
	private const float minTravelTime = 9.0f;
	private const float maxTravelTime = 12.5f;
	private const float minTimeBetweenFlights = 20f;
	private const float maxTimeBetweenFlights = 35f;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine (MoveShip (firstDelay));
	}

	void MoveShip()
	{
		StartCoroutine (MoveShip(Random.Range (minTimeBetweenFlights, maxTimeBetweenFlights)));
	}

	IEnumerator MoveShip(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		float yPos = Random.Range (minYPos, maxYPos);
		float flyTime = Random.Range (minTravelTime, maxTravelTime);
		gameObject.transform.position = new Vector3 (startXPos, yPos, 1);
		iTween.MoveTo (gameObject, iTween.Hash ("x", endXPos, "time", flyTime));
		iTween.ShakePosition (Camera.main.gameObject, iTween.Hash ("amount", new Vector3 (.18f, .18f, 1f), "time", flyTime * .3f));
		SoundManager.Instance.PlaySoundEffect (SoundManager.Instance.sfxShip);
		MoveShip ();
	}

	// Update is called once per frame
	void Update () 
	{
	
	}
}
