﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		float randY = Random.Range (240.0f, 330.0f);
		transform.rotation = Quaternion.Euler (0, randY, 0);
		iTween.RotateBy(gameObject,iTween.Hash("y",1.0f,"speed",3.5f,"looptype",iTween.LoopType.loop));

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
