﻿using UnityEngine;
using System.Collections;

public class ObstacleManager : MonoBehaviour {

	float minYPos = -5.801007f;
	float maxYPos = -0.18465f;
	float pipeStartXPos = 7.4f;
	public const float obstacleSpeed = 3.0f;
	public const int totalGroundPieces = 4;
	
	[SerializeField] GameObject pipeSetPrefab;
	
	float timeBetweenPipes = 3.1f;
	float timeBetweenPipesFirstRun = 2.0f;
	
	void Awake()
	{
		DebugUtil.Assert(pipeSetPrefab != null);
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_FIRST_TAP);
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_END_GAME);
	}
	
	// Use this for initialization
	void Start () 
	{

	}
	
	IEnumerator CreatePipes(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		GameObject pipe = Instantiate(pipeSetPrefab) as GameObject;
		pipe.transform.position = new Vector3(pipeStartXPos,Random.Range(minYPos,maxYPos),pipe.transform.position.z);
		StartCoroutine("CreatePipes",timeBetweenPipes);
		
	}
	
	public void NotifFirstTap()
	{
		StopCoroutine("CreatePipes");
		StartCoroutine("CreatePipes",timeBetweenPipesFirstRun);
	}
	
	public void NotifEndGame()
	{
		StopCoroutine("CreatePipes");
	}

	void OnDestroy()
	{
		//NotificationCenter.DefaultCenter.RemoveObserver(this,Notifications.NOTIF_START_GAME);
	}
}
