﻿using UnityEngine;
using System.Collections;


public class GameManager : MonoBehaviour 
{

	private static GameManager instance;

	public static GameManager Instance {
		get {
			return instance;
		}

	}

	[SerializeField] tk2dTextMesh scoreText;
	[SerializeField] tk2dTextMesh finalScore;
	[SerializeField] tk2dTextMesh bestScoreText;
	[SerializeField] GameObject mainMenuObjects;
	[SerializeField] GameObject gameObjects;
	[SerializeField] GameObject endGameObjects;
	[SerializeField] Collider inputCollider;
	[SerializeField] GameObject tutorialObject;
	[SerializeField] GameObject[] groundPrefab;

	private static bool gameActive;
	private int score;
	private Vector3 endGameMenuStartPos = new Vector3(0,-14.399f,-3.6565f);
	private float endGameEnterYPos = 1.0707f;
	private int bestScore;
	public static float SpeedMultiplier = 1.0f;
	private float speedIncrementVal = .01f;
	private int currentTokens;

	public int Score {
		get {
			return score;
		}
		set {
			score = value;
		}
	}

	public static bool GameActive {
		get {
			return gameActive;
		}
		set {
			gameActive = value;
		}
	}


	void Awake()
	{
		Application.targetFrameRate = -1;
		DebugUtil.Assert(scoreText != null);
		DebugUtil.Assert(mainMenuObjects != null);
		DebugUtil.Assert(gameObjects != null);
		DebugUtil.Assert(endGameObjects != null);
		DebugUtil.Assert(finalScore != null);
		DebugUtil.Assert(inputCollider != null);
		DebugUtil.Assert(bestScoreText != null);
		DebugUtil.Assert(tutorialObject != null);
		instance = this;

//#if UNITY_IPHONE
		//CBBinding.init("530bb5242d42da0176adaf04","485d55e127cd8c12d0ddb91cf15fb751e3c69ef9");
//#endif
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_END_GAME);
		NotificationCenter.DefaultCenter.AddObserver(this,Notifications.NOTIF_FIRST_TAP);
	}

	// Use this for initialization
	void Start () 
	{
		;
//#if UNITY_IPHONE
		//CBBinding.showInterstitial(null);
//#endif
		SoundManager.Instance.PlayMusic(SoundManager.Instance.musicMain);

		LoadPrefs();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddToken()
	{
		currentTokens++;
	}

	public void AddScore()
	{
		IncreaseSpeed();
		score++;
		scoreText.text = score.ToString();
		scoreText.Commit();
	}

	public void IncreaseSpeed()
	{
		SpeedMultiplier += speedIncrementVal;
	}

	public void ResetSpeed()
	{
		SpeedMultiplier = 1.0f;
	}
	
	public void ButtonPressed(string buttonName)
	{
		if (buttonName == "playButton")
		{
			SoundManager.Instance.PlaySoundEffect(SoundManager.Instance.sfxPlayButton);
			GameSetup();
			NotificationCenter.DefaultCenter.PostNotification(this,Notifications.NOTIF_RESTART_GAME);
			NotifRestartGame();
		}
		else if (buttonName == "restartButton")
		{
			SoundManager.Instance.StopSoundFX();
			NotificationCenter.DefaultCenter.PostNotification(this,Notifications.NOTIF_RESTART_GAME);
			NotifRestartGame();
		}
		else if (buttonName == "freeGamesButton")
		{

		}
		else if (buttonName == "mainScreenButton")
		{
			mainMenuObjects.SetActive(true);
			gameObjects.SetActive(false);

			NotificationCenter.DefaultCenter.PostNotification(this,Notifications.NOTIF_MAIN_SCREEN);
			SoundManager.Instance.StopSoundFX();
			SoundManager.Instance.PlayMusic(SoundManager.Instance.musicMain);
		}
		else if (buttonName == "leaderboardButton")
		{
			LeaderboardManager.ShowLeaderboards();
		}
	}

	public void GameSetup()
	{
		GameManager.GameActive = true;
		mainMenuObjects.SetActive(false);
		gameObjects.SetActive(true);
		score = 0;
		scoreText.text = "0";
		scoreText.Commit();
	}
	

	public void NotifRestartGame()
	{
//#if UNITY_IPHONE
		//AdMobPlugin.CreateBannerView("4063865121090889",AdMobPlugin.AdSize.Banner,false);
		//AdMobPlugin.ShowBannerView();
//#endif
		endGameObjects.transform.position = endGameMenuStartPos;
		CreateGround ();
		ResetSpeed ();
		GameSetup();
		inputCollider.enabled = true;
		NotificationCenter.DefaultCenter.PostNotification(this,Notifications.NOTIF_START_GAME);
		tutorialObject.SetActive(true);
		if (!SoundManager.Instance.musicSource.isPlaying)
			SoundManager.Instance.PlayMusic(SoundManager.Instance.musicMain);


	}

	public void CreateGround()
	{
		for (int x = 0; x < ObstacleManager.totalGroundPieces; x++) 
		{
			GameObject fab;

			if (x % 2 == 0)
				fab = groundPrefab[0];
			else
				fab = groundPrefab[1];

			GameObject ground = Instantiate(fab) as GameObject;
			Obstacle obstacle = ground.GetComponent<Obstacle>();
			Renderer oRender = obstacle.GetComponentInChildren<Renderer>();

			if (x == 0)
			{
				ground.transform.position = obstacle.GroundStartPos;
			}
			else
			{
				float newX = obstacle.GroundStartPos.x + (oRender.bounds.size.x * x);
				ground.transform.position = new Vector3(newX,obstacle.GroundStartPos.y,obstacle.GroundStartPos.z);
			}
		}
	}

	public void NotifEndGame()
	{
		if (score > bestScore)
		{
			bestScore = score;
			LeaderboardManager.ReportScore(bestScore);
		}

		SavePrefs();

		bestScoreText.text = "best score: " + bestScore.ToString();
		bestScoreText.Commit();

		inputCollider.enabled = false;
		GameActive = false;
		StartCoroutine(EndGameMenu(1.0f));

	}

	IEnumerator EndGameMenu(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		SoundManager.Instance.PlaySoundEffect(SoundManager.Instance.sfxGameOver);
		endGameObjects.SetActive(true);
		iTween.MoveTo(endGameObjects,iTween.Hash("y",endGameEnterYPos,"time",1.0f,"easetype",iTween.EaseType.easeOutBounce));
		finalScore.text = "score: " + score.ToString();
		finalScore.Commit();
	}

	public void SavePrefs()
	{
		PlayerPrefs.SetInt("bestscore",bestScore);
	}

	public void LoadPrefs()
	{
		bestScore = PlayerPrefs.GetInt("bestscore");
	}
	
	void ProcessAuthentication(bool success)
	{
		if (success)
		{

		}
		else
		{

		}
	}





	public void NotifFirstTap()
	{
		tutorialObject.SetActive(false);
	}
}
