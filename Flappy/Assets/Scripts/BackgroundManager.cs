﻿using UnityEngine;
using System.Collections;

public class BackgroundManager : MonoBehaviour {

	[SerializeField] GameObject farBackgroundPrefab;
	[SerializeField] GameObject midBackgroundPrefab;
	[SerializeField] GameObject closeBackgroundPrefab;

	[SerializeField] GameObject farBackground;
	[SerializeField] GameObject midBackground;
	[SerializeField] GameObject closeBackground;

	private const float farBGSpeed = .5f;
	private const float midBGSpeed = 1.0f;
	private const float closeBGSpeed = 1.8f;
	private const float startXPos = 30.508f;
	private const float destroyXPosFar = -15f;

	private GameObject midBackground2;
	private GameObject farBackground2;
	private GameObject closeBackground2;
	
	void Awake()
	{

	}

	// Use this for initialization
	void Start () 
	{
		//midBackground2 = Instantiate (midBackgroundPrefab) as GameObject;
		//midBackground2.transform.position = new Vector3 (midBackground.transform.position.x + midBackground.renderer.bounds.size.x, midBackground.transform.position.y, midBackground.transform.position.z);

		//farBackground2 = Instantiate (farBackgroundPrefab) as GameObject;
		//farBackground2.transform.position = new Vector3 (farBackground.transform.position.x + farBackground.renderer.bounds.size.x, farBackground.transform.position.y, farBackground.transform.position.z);

		//closeBackground2 = Instantiate (closeBackgroundPrefab) as GameObject;
		//closeBackground2.transform.position = new Vector3 (closeBackground.transform.position.x + closeBackground.renderer.bounds.size.x, closeBackground.transform.position.y, farBackground.transform.position.z);


	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!GameManager.GameActive)
			return;

		midBackground.transform.Translate(Vector3.left * midBGSpeed * Time.deltaTime);
		//midBackground2.transform.Translate(Vector3.left  * midBGSpeed * Time.deltaTime);

		//farBackground.transform.Translate(Vector3.left * farBGSpeed * Time.deltaTime);
		//farBackground2.transform.Translate(Vector3.left * farBGSpeed * Time.deltaTime);

		//closeBackground.transform.Translate (Vector3.left * closeBGSpeed * Time.deltaTime);
		//closeBackground2.transform.Translate (Vector3.left * closeBGSpeed * Time.deltaTime);

		//if (farBackground.transform.position.x <= destroyXPosFar)
			//farBackground.transform.position = new Vector3 (farBackground2.transform.position.x + farBackground.renderer.bounds.size.x, farBackground.transform.position.y, farBackground.transform.position.z);

		//if (farBackground2.transform.position.x <= destroyXPosFar)
			//farBackground2.transform.position = new Vector3 (farBackground.transform.position.x + farBackground.renderer.bounds.size.x, farBackground.transform.position.y, farBackground.transform.position.z);

		if (midBackground.transform.position.x <= destroyXPosFar)
			midBackground.transform.position = new Vector3 (startXPos, midBackground.transform.position.y, midBackground.transform.position.z);

		//if (midBackground2.transform.position.x <= destroyXPosFar)
			//midBackground2.transform.position = new Vector3 (midBackground.transform.position.x + midBackground.renderer.bounds.size.x, midBackground.transform.position.y, midBackground.transform.position.z);

		//if (closeBackground.transform.position.x <= destroyXPosFar)
			//closeBackground.transform.position = new Vector3 (closeBackground2.transform.position.x + closeBackground.renderer.bounds.size.x, closeBackground.transform.position.y, closeBackground.transform.position.z);

		//if (closeBackground2.transform.position.x <= destroyXPosFar)
			//closeBackground2.transform.position = new Vector3 (closeBackground.transform.position.x + closeBackground.renderer.bounds.size.x, closeBackground2.transform.position.y, closeBackground2.transform.position.z);
	}


}
